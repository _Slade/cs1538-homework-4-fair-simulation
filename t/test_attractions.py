import unittest

from attractions import *

class TestMerryGoRound(unittest.TestCase):

    def setUp(self):
        self.mgr = MerryGoRound()

    def test_cost(self):
        self.assertEqual(self.mgr.get_cost(), 8)

    def test_capacity(self):
        self.assertEqual(self.mgr.get_capacity(), 20)

class TestRollerCoaster(unittest.TestCase):

    def setUp(self):
        self.rc = RollerCoaster()

    def test_cost(self):
        self.assertEqual(self.rc.get_cost(), 16)

    def test_capacity(self):
        self.assertEqual(self.rc.get_capacity(), 60)

class TestStallsAndGames(unittest.TestCase):

    def setUp(self):
        self.sag = StallsAndGames()

    def test_cost(self):
        self.assertEqual(self.sag.get_cost(), 1)

    def test_capacity(self):
        self.assertEqual(self.sag.get_capacity(), 50)

    def test_occupants(self):
        self.assertEqual(self.sag.get_occupants(), 0)

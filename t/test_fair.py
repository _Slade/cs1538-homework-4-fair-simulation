"Unit tests for fair.py."

import unittest

import event
import fair

from event import EventQueue
from fair import *

class TestFair(unittest.TestCase):

    def setUp(self):
        self.events = EventQueue()
        self.fair = Fair(
            events=self.events,
            early_cust_num=200,
            early_cust_mean=0,
            early_cust_std_dev=15 * 60,
            late_cust_rate=15 / (60*60)
        )

    def test_init_makes_customers(self):
        self.assertGreaterEqual(len(self.events), 200)

# End of TestFair

class TestTicketBooth(unittest.TestCase):

    def test_close(self):
        tb = TicketBooth()
        customers = [ Customer() for _ in range(0, 100) ]
        time = 5
        for customer in customers:
            next_event = tb.enter_line(customer, time)
            self.assertGreater(
                next_event.get_time(),
                time,
                "Next event must happen later than first"
            )

        self.assertTrue(
            tb.people_in_line() == len(customers),
            "The ticket booth should have everyone in line that went to get"
            " tickets"
        )

        tb.close()
        self.assertTrue(
            tb.people_in_line() == 0,
            "The lines should be empty once the ticket booth closes"
        )

# End of TestTicketBooth

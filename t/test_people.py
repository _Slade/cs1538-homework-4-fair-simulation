"""Unit tests for the people classes."""

import unittest

import people
from people import *

class TestCustomer(unittest.TestCase):

    def test_init(self):
        customer = Customer() # Customer who arrived after 5 minutes
        self.assertTrue(True, "Constructor shouldn't throw exceptions")

    def test_buy_tickets(self):
        customer = Customer()
        customer.buy_tickets(10)
        self.assertEqual(customer.tickets_held(), 10)

    def test_spend_tickets(self):
        customer = Customer()
        customer.buy_tickets(10)
        customer.spend_tickets(6)
        self.assertEqual(customer.tickets_held(), 4)

    def test_tickets(self):
        customer = Customer()
        self.assertEqual(customer.tickets_held(), 0)

    def test_buy_noninteger_tickets(self):
        customer = Customer()
        self.assertRaises(ValueError, customer.buy_tickets, 1.5)

    def test_buy_negative_tickets(self):
        customer = Customer()
        self.assertRaises(ValueError, customer.buy_tickets, -1)

    def test_buy_zero_tickets(self):
        customer = Customer()
        self.assertRaises(ValueError, customer.buy_tickets, 0)

    def test_spend_too_many_tickets(self):
        customer = Customer()
        customer.buy_tickets(15)
        self.assertRaises(Customer.OutOfTicketsError, customer.spend_tickets, 20)

    def test_is_early_one(self):
        customer = Customer(is_late=False)
        self.assertTrue(customer.is_early())

    def test_is_early_two(self):
        customer = Customer(is_late=True)
        self.assertFalse(customer.is_early())

# End of TestCustomer

class TestEmployee(unittest.TestCase):
    ...
# End of TestEmployee

class TestCashier(unittest.TestCase):
    ...
# End of TestCashier

if __name__ == "__main__":
    unittest.main()

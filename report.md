---
title: |
    CS1538 Homework 4
    <https://github.com/PittCS1538/csc34-hw4>
author:
  - Christopher S. Constantino (Chris-Slade)
date: 5 April 2017
documentclass: article
geometry: margin=1in
papersize: letter
indent: true
fontsize: 12pt
linestretch: 1.0
mainfont: Linux Libertine O
mathfont: Linux Libertine O
numbersections: true
secnumdepth: 2
---

\newpage

# Design of the Simulation

This section documents the design choices made for the implementation of the
simulation.

## Customer Arrival

200 customers arrive at around 10AM, and their arrival times are distributed
normally around 10AM with a standard deviation of 15 minutes. Those who are
early (the left half of the distribution) are forced to wait until one second
after 10AM to enter the park.

## Obtaining Tickets

When customers go to choose a line, they will estimate the wait time of the
cash line as the average wait time the line would have for a certain number
of people. With the desired ticket number distributed normally with a mean
of 100 and a standard deviation of 30, and the base wait time distributed as
$\mathop{Exp}(2\text{ minutes})$, the average wait time is equal to the number
of customers in the line times 130 seconds.

It is possible to use prior knowledge of ticket-buying decisions to calculate
the exact wait time of a given cash line, but not only would it complicate
the simulation to do so (by requiring the number of tickets wanted for each
customer to be chosen beforehand), it is unlikely that customers at a real fair
would know the actual wait time, and would instead guess based on the length
of the line and how quickly it moves on average. As such, the customers in my
simulation guess based on averages as well.

## Attractions

### Rollercoaster and Merry-Go-Round

Both the rollercoaster and merry-go-round remain idle until they reach their
maximum capacity, at which point any other arriving customers are made to wait
in line until the ride finishes. (In reality, the rides would be run under
capacity during less busy times of the day, but this is difficult to simulate.)
When it does, the riders all disembark, and only after the ride has been fully
cleared does the next load of riders begin to embark.

A simplifying assumption was used to implement passenger boarding. Customers
who get on either ride to wait for the next run do not have their boarding
times simulated unless they are the very last to get on.

If we allow customers to board truly in parallel, there is a nonzero
probability that those who arrive before the last customer (as in, the customer
whose boarding would fill the ride to capacity) will nevertheless take longer
to board. This means that in parallel boarding the distribution of times
between the last customer arriving and the ride starting its run is in fact
a complex multivariate distribution made up of exponentially distributed
board times shifted by (mostly) exponentially distributed arrival times,
since customers leaving other attractions do so following an exponential
distribution. (It's even more complex than that, since customers may arrive
after leaving either of the ticket booth lines.)

In other words, if the $n$^th^ (for a ride with capacity $n$) customer arrives
and takes 60 seconds to board, then 10 seconds later, the $n+1$^th^ customer
arrives and would take 20 seconds to board, should the $n+1$^th^ customer be
allowed to "cut ahead" and board? Under my assumption, this is not allowed to
happen, and the $n$^th^ arrival since the last run of the rollercoaster will
always be permitted to board for the next run. More simply put, customers line
up in FIFO order to get on the ride, and board in parallel when the ride stops
or is stopped.

### Stalls and Games

Customers are permitted to play these in parallel, and there is no wait for the
game to start or coordination between players. (Think of shooting galleries,
whack-a-mole, skee ball, the water gun game, etc. --- all function in this
manner.)

## Statistics Collected

The following daily statistics were collected:

* Number of customers who were too late to buy tickets
* Expense (i.e. money spent)
* Merry-go-round runs
* Merry-go-round visits
* Number of customers (i.e. the total number of people who visit the fair
throughout the day)
* Rollercoaster runs
* Rollercoaster visits
* Revenue
* Stalls and games visitors
* Tickets wasted

The following hourly statistics were collected, and averaged by hour over each
of the days of the simulation:

* Cash line length
* Express line length
* Cash line wait time
* Express line wait time
* Rollercoaster line length
* Merry-go-round line length
* Customers present

In addition, the total profit made over one summer (100 days) was collected.

# Experimental Design
<!--
Your report should explain your experimental design in detail. Give
justifications for your choices. Describe enough details about your
experimental decisions such that another student could replicate your
experiment using [his] own simulation implementation.
-->

For the experimental portion, I decided to focus on two attractions, the stalls
and games and the merry-go-round. The questions I'm interested in are:

* How does increasing or decreasing the merry-go-round's capacity affect
the fair's profits? How does it affect the merry-go-round's line length?
* How does increasing or decreasing the number of employees/stalls affect the
number of tickets wasted? How does it affect the fair's profits?

I began by simulating 100 days of the fair with the default parameters.
Then for the merry-go-round I ran simulations with its capacity decreased
to 15, then increased to 30. Then I focused on the stalls and games,
running simulations with 1 to 5 fewer employees/stalls, then 1 to 5 more
employees/stalls.

# Results
<!--
After running the experiments, you should present the results in a clear and
easy to understand way. Use tables and graphs as necessary. Some issues you
should think about and address in the report:
  ● How good is the default setting? Without changing anything, is the system
  stable (i.e., no line grows out of hand)? Is the local fair making money?
  ● Assuming that you cannot change customer arrivals or behaviors, does
  changing the fair infrastructure (ticket booths, attractions) improve our two
  objectives?
  ● What if we *can* advertise to get more customers to come (should we
  target Early Customers or Late Customers?), or somehow convince them to
  prefer some attractions over others. How would these modifications change the
  bottom line?
-->

All confidence intervals (e.g. $x \pm y$) were calculated with a Student's
$t$-test at $.95$ confidence. For hypothesis testing I used a two-means
$t$-test with unknown population standard deviation and known sample standard
deviation.

## The Default Setting

### Costs, Revenue, and Profits

The fair employs 13 employees at $\$10/$hour. 12 of them work for 8 hours a
day, and the 13th only works 7 hours. The cost per day in wages is $\$960 +
\$70 = \$1,030$. The cost for an entire summer is 100 times that, or \$103,000.

However, the amount of revenue the fair makes vastly outstrips its expenses.
On average, the fair sells $44,437\pm258$ tickets per day, amounting
to $\$11,109.31\pm64.56$ of revenue. This comes out to an average of
$\$10,079.31\pm64.56$ in profit. Over the entire summer I simulated, the fair
makes a profit of approximately $\$1,007,931$.

### Customer Experience

On average, $282.2\pm1.56$ customers visit the fair in a day. Of them,
$20.46\pm1.06$ arrive too late to purchase any tickets. Furthermore, an average
of $307.89\pm54.19$ tickets (worth $\$76.97\pm13.55$) are wasted each day.

### Attractions

The stalls and games are by far the most commonly visited attraction, followed
by the rollercoaster and then the merry-go-round. Stalls and games also have
the most variability between days, with a standard deviation of 2,778.37
visits, compared to the rollercoaster's 48.99 and the merry-go-round's 39.


: Attractions visited

| Attraction       | Times visited        |
|------------------|---------------------:|
| Rollercoaster    | $1,841.06\pm9.72$    |
| Merry-Go-Round   | $1,215.73\pm7.93$    |
| Stalls and Games | $18,775.65\pm551.29$ |

### Lines


: Average line length throughout the day

|Line   |11 AM| 12 AM|  1 PM|  2 PM|  3 PM|  4 PM|  5 PM|  6 PM|
|-------|----:|-----:|-----:|-----:|-----:|-----:|-----:|-----:|
|Cash   |  0.0| 21.92| 17.92| 19.95| 22.77| 25.22| 28.20| 10.49|
|Express|  0.0| 11.47|  2.84|  2.94|  3.19|  2.96|  3.39|  0.00|
|RC     |  0.0| 22.26| 15.00| 21.46| 12.35| 10.79| 12.01|  8.72|
|MGR    |  0.0|113.09|141.47|146.72|149.51|143.05|130.52|104.69|


: Average wait times in ticket lines in seconds

|Line   |11 AM|  12 AM|   1 PM|   2 PM|   3 PM|   4 PM|   5 PM|   6 PM|
|-------|----:|------:|------:|------:|------:|------:|------:|------:|
|Cash   |  0.0|2849.60| 2329.6| 2593.5|2960.10| 3278.6|3666.00| 1363.7|
|Express|  0.0| 172.05|   42.6|   44.1|  47.85|   44.4|  50.85|    0.0|

## Changing the Merry-Go-Round
<!--
How does increasing or decreasing the merry-go-round's capacity affect the
fair's profits? How does it affect the merry-go-round's line length?
-->

Interestingly, there was a slight decrease in average line length throughout
the day with both the increase and decrease to the merry-go-round's capacity.
This may be the result of random variation, because it doesn't make much sense
as a trend.

Reducing the merry-go-round's capacity from 20 to 15 caused a statistically
significant drop-off in the average number of visitors, from 1,215.73 to
933.45. Increasing its capacity from 20 to 30 caused an increase, from 1,215.73
to 1,289.62.


: Changes in line length from default merry-go-round capacity

Capacity | 11 AM | 12 AM  | 1 PM   | 2 PM   | 3 PM   | 4 PM   | 5 PM    | 6 PM
---------|-------|--------|--------|--------|--------|--------|---------|--------
30       | 0.0   | -4.99  | -7.89  | -15.7  | -34.28 | -73.07 | -118.45 | -105.72
15       | 0.0   | -2.82  | -3.41  | -4.52  | -10.66 | -19.62 | -31.59  | -52.2

Neither increasing the capacity nor decreasing it caused a statistically
significant increase in profits ($p = 0.86$ and $0.44$, respectively). Of
the two, downgrading the size of the carousel had the better short-term (one
summer) return on investment, though the standard carousel size was better than
either alternative.

## Changing the Stalls and Games
<!--
How does increasing or decreasing the number of employees/stalls affect the
number of tickets wasted? How does it affect the fair's profits?
-->

Unfortunately, the number of tickets wasted is so highly variable across days
that there no clear trend can be gleaned from the samples taken. The results
for eleven different scenarios were tested, each with a different number of
stalls and employees, and are shown in the table below.


: Tickets wasted on average by number of additional stalls and games

Stall adjustment | Avg. tix. wasted | Std. dev. | Diff. from default
-----------------|------------------|-----------|------
-5               | 315.60           | 338.8921  | +7.71
-4               | 247.80           | 205.9791  | -60.09
-3               | 337.68           | 338.5086  | +29.79
-2               | 300.73           | 286.5088  | -7.16
-1               | 296.58           | 296.3167  | -11.31
+0               | 307.89           | 273.1281  | +0
+1               | 355.71           | 322.7282  | +47.82
+2               | 308.23           | 201.1339  | +0.34
+3               | 362.79           | 359.4995  | +54.9
+4               | 321.55           | 284.5560  | +13.66
+5               | 324.94           | 279.6662  | +17.05

There is a consistent trend toward higher profits gained by eliminating stalls
and their employees. It is likely that this trend will reverse after a certain
threshold, because the stalls and games see a large amount of use, but for all
values I tested, the fewer employees, the higher the profit. The results are
shown in the table below.


: Effect of changing the number of stalls/employees on profit

Stall adjustment | Avg. profit | Std. dev. | Profit increased? | p-value
-----------------|-------------|-----------|-------------------|---------
-5               | \$10,488.92 | \$311.17  | Y                 | 0
-4               | \$10,363.05 | \$330.97  | Y                 | 0
-3               | \$10,307.09 | \$347.81  | Y                 | 0
-2               | \$10,211.39 | \$315.81  | Y                 | 0.002
-1               | \$10,149.78 | \$306.57  | N                 | 0.058
+1               | \$9,991.31  | \$328.03  | N                 | 0.971
+2               | \$9,925.89  | \$339.86  | N                 | 0.999
+3               | \$9,813.77  | \$332.08  | N                 | 1
+4               | \$9,743.38  | \$342.90  | N                 | 1
+5               | \$9,656.74  | \$320.90  | N                 | 1

# Conclusions
<!--
Based on your experimental analyses, draw conclusions about what the local fair
should do.
-->

Firstly, the fair should not change the capacity of the merry-go-round. If it
does, it should consider increasing its size, which may result in long-term
payoffs at the expense of a short-term dip in profits.

Secondly, the fair should consider reducing the number of games and stalls by
at least two and up to five to save on labor costs.

Finally, the fair should pay me to conduct additional research in order to more
accurately model ticket waste, so that I can offer more conclusive advice on
how to improve the customer experience.

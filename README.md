# README

To run the simulation, run `sim.py` with a Python 3 interpreter. With the
default settings it can take a while to run (just under a minute on my fairly
powerful laptop) since it simulates 100 days, so you might want to change that
with the `--days` option.

A full list of options and what they do can be obtained by running `sim.py`
with the `--help` flag.

Setting the logging level (with the `-l` option) to `DEBUG` or `INFO` will
show in more detail what the simulation is doing. `INFO` is probably the level
you want, since `DEBUG` is extremely verbose. All logging is printed to the
standard error stream.

At the end of the simulation, a JSON file will be created with the full
statistics gathered from the simulation, and aggregated versions of the
statistics (i.e. averages and totals) will be printed to standard output.

You may also run the [run_experiments.sh](run_experiments.sh) script at your
own peril --- it runs about a dozen simulations with different parameters
concurrently and saves their results.

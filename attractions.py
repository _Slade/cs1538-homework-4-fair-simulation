import logging
import numpy.random as npr

from collections import deque
from random import random as rand

import util

from event import RideFinished, VisitAttraction

class Attraction(object):
    """Attractions work as follows (pseudocode):

        customer.buy_tickets()
        attraction = choose_attraction()
        next_event = attraction.get_on_ride(customer)
        if next_event is None:
            do nothing
        else:
            events.push(next_event)
        ...
        event = events.pop()
        if event is a RideFinished:
            for new_event in event.get_attraction().finish_run():
                events.push(new_event)
    """

    def __init__(self):
        # Class logger
        self.__class__.LOGGER = logging.getLogger(self.__class__.__name__)
        # Customers who are waiting in line to embark
        self._line = deque()
        # Customers who are actually on the ride (i.e. who have embarked)
        self._ride = deque()
        # Is the ride currently running?
        self._is_running = False
        # Number of people who visit the attraction
        self._total_visitors = 0

    def get_visitors(self):
        return self._total_visitors

    def get_cost(self):
        raise NotImplementedError("Attraction is abstract")

    def get_capacity(self):
        raise NotImplementedError("Attraction is abstract")

    def get_ride_time(self):
        raise NotImplementedError("Attraction is abstract")

    def get_embark_time_offset(self):
        raise NotImplementedError("Attraction is abstract")

    def get_disembark_time_offset(self):
        raise NotImplementedError("Attraction is abstract")

# End of Attraction

class RideWithQueue(Attraction):

    def __init__(self):
        super().__init__()
        self._times_run = 0

    def get_times_run(self):
        return self._times_run

    def get_line_length(self):
        if not self._is_running:
            return len(self._line)
        else:
            return len(self._line) + len(self._ride)

    def get_on_ride(self, customer, time):
        """There are two scenarios. If the customer arrives before the current
        ride finishes, the earliest time he can board is `_current_ride_end`.
        If he arrives after the current ride has finished, he can immediately
        try to board then. The base boarding time is therefore `max(time,
        _current_ride_end)`, which then is offset by 1.5 seconds plus some X ~
        Exp(30s).
        """
        assert customer.tickets_held() >= self.get_cost(), \
            '{} needs {} tickets, only has {}'.format(
                customer,
                self.get_cost(),
                customer.tickets_held()
            )
        assert len(self._ride) <= self.get_capacity(), 'Ride went over capacity'

        if self._is_running:
            self._line.appendleft(customer)
            self.LOGGER.debug(
                '%s lined up at %s to wait for ride to finish',
                customer,
                util.sec_to_tod(time)
            )
        else:
            assert len(self._ride) < self.get_capacity(), \
                'Ride should be running when at capacity'
            self._ride.appendleft(customer)
            self._total_visitors += 1
            customer.spend_tickets(self.get_cost())
            self.LOGGER.debug(
                '%s boarded ride at %s (%d tickets remaining)',
                customer,
                util.sec_to_tod(time),
                customer.tickets_held()
            )
            if len(self._ride) == self.get_capacity():
                # Only need to consider the boarding time of the last to board
                offset = self.get_embark_time_offset()
                finish_time = time + offset + self.get_ride_time()
                self.LOGGER.debug(
                    'Ride starting at %s, finishes at %s (%d waiting)',
                    util.sec_to_tod(finish_time),
                    util.sec_to_tod(time + offset),
                    len(self._line)
                )
                self._is_running = True
                self._times_run += 1
                return RideFinished(
                    time=finish_time,
                    customer=None,
                    attraction=self
                )
        return None

    def finish_run(self, event):
        self.LOGGER.debug(
            'Ride finished at %s',
            util.sec_to_tod(event.get_time())
        )
        assert len(self._ride) == self.get_capacity()

        # Remove all riders and send them to another attraction
        time = event.get_time()
        max_disembark_time = 0
        for _ in range(0, self.get_capacity()):
            assert self._ride, 'Ride should not be empty'
            customer = self._ride.pop()
            self.LOGGER.debug(
                '%s disembarked at %s (%d tickets remaining)',
                customer,
                util.sec_to_tod(event.get_time()),
                customer.tickets_held()
            )
            disembark_time = time + self.get_disembark_time_offset()
            if disembark_time > max_disembark_time:
                max_disembark_time = disembark_time
            yield VisitAttraction(time=disembark_time, customer=customer)
        # Take customers from the line and put them on the next ride. Customers
        # can only start getting on once the last customer from the previous
        # ride got off.
        self._is_running = False
        while self._line and len(self._ride) < self.get_capacity():
            next_event = self.get_on_ride(self._line.pop(), max_disembark_time)
            if next_event is not None:
                yield next_event

# End of RideWithQueue

class RollerCoaster(RideWithQueue):

    def get_cost(self):
        return 16

    def get_capacity(self):
        return 60

    def get_ride_time(self):
        return 3 * 60 # 3 minutes

    def get_embark_time_offset(self):
        # Cheat by doing customers 1 at a time, since the drawn numbers will
        # average out anyway.
        return 1.5 + npr.exponential(30)

    def get_disembark_time_offset(self):
        return npr.exponential(30)

# End of RollerCoaster

class MerryGoRound(RideWithQueue):

    DEFAULT_CAPACITY = 20

    def __init__(self, capacity=None):
        super().__init__()
        if capacity is None:
            capacity = self.DEFAULT_CAPACITY
        assert capacity > 0, 'Capacity must be nonnegative'
        self._capacity = capacity

    def get_cost(self):
        return 8

    def get_capacity(self):
        return self._capacity

    def get_ride_time(self):
        return 4 * 60 # 4 minutes

    def get_embark_time_offset(self):
        return npr.exponential(2 * 60) # 2 minutes on average

    def get_disembark_time_offset(self):
        return npr.exponential(30) # 30 seconds on average

# End of MerryGoRound

class StallsAndGames(Attraction):

    DEFAULT_CAPACITY = 50

    def __init__(self, capacity=None):
        super().__init__()
        if capacity is None:
            capacity = self.DEFAULT_CAPACITY
        assert capacity > 0, 'Capacity must be nonnegative'
        self._capacity = capacity
        self._occupants = 0

    def get_cost(self):
        return 1

    def get_capacity(self):
        assert self._capacity is not None
        return self._capacity

    def get_occupants(self):
        return self._occupants

    def get_ride_time(self):
        return npr.exponential(30)

    def get_on_ride(self, customer, time):
        # If the stalls are full the customer just goes somewhere else
        if self._occupants >= self.get_capacity():
            self.LOGGER.debug(
                "%s left stalls and games because it's full at %s",
                customer,
                util.sec_to_tod(time)
            )
            return VisitAttraction(time=time + 1, customer=customer)
        # Otherwise the customer plays for a bit then leaves
        self._occupants += 1
        self._total_visitors += 1
        play_time = time + self.get_ride_time()
        self.LOGGER.debug(
            '%s playing at stalls and games until %s',
            customer,
            util.sec_to_tod(play_time)
        )
        return RideFinished(time=play_time, customer=customer, attraction=self)

    def finish_run(self, event):
        self._occupants -= 1
        yield VisitAttraction(
            customer=event.get_customer(),
            time=event.get_time()
        )

# End of StallsAndGames

def random_attraction(rollercoaster, stalls_and_games, merry_go_round, tickets):
    """Customers choose attractions following the distribution:

         Attraction | Can afford? | P
        ------------+-------------+-----
         RC         | Y           | 40%
         S&G        | Y           | 35%
         MGR        | Y           | 25%
        ------------+-------------+-----
         RC         | N           | 0%
         S&G        | Y           | 58%
         MGR        | Y           | 42%
        ------------+-------------+-----
         RC         | N           | 0%
         S&G        | Y           | 100%
         MGR        | N           | 0%
        ------------+-------------+-----
         RC         | N           | 0%
         S&G        | N           | 0%
         MGR        | N           | 0%
    """
    assert rollercoaster.get_cost() \
        > merry_go_round.get_cost() \
        > stalls_and_games.get_cost() \
        > 0
    choice = rand()
    if tickets >= rollercoaster.get_cost():
        if choice < 0.40:
            return rollercoaster
        elif choice < 0.75:
            return stalls_and_games
        else:
            return merry_go_round
    elif tickets >= merry_go_round.get_cost():
        if choice < 0.58:
            return stalls_and_games
        else:
            return merry_go_round
    elif tickets >= stalls_and_games.get_cost():
        return stalls_and_games
    else:
        return None

"""People who can be at the fair."""

class Customer(object):
    """Represents a customer."""

    _number = 1 # ID number of customer.

    class OutOfTicketsError(Exception):
        """Thrown when a customer attempts to spend tickets he doesn't have."""
        pass

    class BadStateError(Exception):
        pass

    def __init__(self, is_late=False):
        self._is_late = is_late
        self._tickets = 0
        self._number  = Customer._number
        Customer._number += 1

    def buy_tickets(self, number):
        """Buy a number of tickets."""
        if number <= 0:
            raise ValueError("Can't buy 0 or fewer tickets")
        if number != int(number):
            raise ValueError("Can't have non-integer no. of tickets")
        self._tickets += number

    def spend_tickets(self, number):
        if number > self.tickets_held():
            raise Customer.OutOfTicketsError(
                "Customer does not have enough tickets"
            )
        self._tickets -= number
        assert self._tickets >= 0

    def tickets_held(self):
        return self._tickets

    def get_number(self):
        return self._number

    def is_early(self):
        return not self._is_late

    def is_late(self):
        return self._is_late

    def __repr__(self):
        return str(self)

    def __str__(self):
        return "Customer {:3d}".format(self._number)

# End of Customer

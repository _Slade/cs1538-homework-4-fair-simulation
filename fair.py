"""The main object of the simulation."""

import logging

from numpy import random as npr
from random import random as rand

import event
import util

from people import Customer

class Fair(object):
    logger = logging.getLogger('Fair')

    def __init__(
        self,
        events,
        early_cust_num,
        early_cust_mean,
        early_cust_std_dev,
        late_cust_rate
    ):
        self.logger.info(
            "Early customers: %d, "
            "early customer mean time: %d, "
            "early customer std dev: %d, "
            "late customer rate: %f",
            early_cust_num,
            early_cust_mean,
            early_cust_std_dev,
            late_cust_rate
        )

        self._num_customers = 0

        # Draw 200 early customers, distributed normally about 10AM.
        for offset in npr.normal(
            loc=early_cust_mean,
            scale=early_cust_std_dev,
            size=early_cust_num
        ):
            if offset <= 0:
                offset = 1
            events.push(event.Arrival(time=offset, customer=Customer()))
            self._num_customers += 1
        self.logger.info("Generated arrival times for early customers")

        # Draw late customers
        late_cust_start = util.tod_to_sec("10:30:00 AM")
        late_cust_end = util.tod_to_sec("04:00:00 PM")
        # Late arrivals follow a Poisson process, so we can find their times
        # using an exponential distribution.
        time = 0 # Start of late arrival period (10:30AM till 4PM)
        while True:
            arrival_time = npr.exponential(scale=1/late_cust_rate)
            time += arrival_time
            if time < late_cust_end - late_cust_start:
                events.push(
                    event.Arrival(
                        # Arrival times are from 0, need to offset them by the
                        # start of the late arrival period.
                        time=time + late_cust_start,
                        customer=Customer(is_late=True)
                    )
                )
                self._num_customers += 1
            else:
                break # No more late arrivals at the end of the period
        self.logger.info("Generated arrival times for late customers")

    def get_customer_count(self):
        return self._num_customers

class TicketBooth(object):
    """Represents the ticket booth.

    Has a cash line and an express line.

    Cash line sells any number of tickets for $0.25 each at 1 second per 10
    minutes bought plus a base time of 2 minutes, modeled by an exponential.

    Closes at 5PM (420 minutes after opening).
    """
    logger = logging.getLogger('TicketBooth')

    CLOSING_TIME = util.tod_to_sec("05:00:00 PM")
    CASH = 0
    EXPRESS = 1

    def __init__(self):
        self._cash_line         = 0
        self._express_line      = 0
        self._cash_line_wait    = 0
        self._express_line_wait = 0
        self._money_made        = 0
        self._tickets_sold      = 0
        self._is_open           = True

    def enter_line(self, customer, time):
        """Line up to buy tickets.

        The customer chooses a line, then a GetTickets event is returned to
        indicate when the customer gets tickets. When that event happens,
        leave_line() should be called on the customer.
        """
        assert customer.tickets_held() == 0, "Customer already has tickets"
        # Invariant: If there's anyone in either line, the line's wait time
        # must be greater than zero.
        assert not self._express_line > 0 or self._express_line_wait > 0
        assert not self._cash_line > 0 or self._cash_line_wait > 0

        # Choose the line to enter (cash or express)
        chosen_line = self._choose_line(customer)

        # Compute the desired number of tickets, how long it will take to get
        # them, and create the event to indicate when the sale takes place.
        next_event      = None
        time_offset     = None
        desired_tickets = None
        if   chosen_line == TicketBooth.CASH:
            desired_tickets = TicketBooth._get_cash_desired_tickets()
            time_offset     = TicketBooth._get_cash_time_offset(desired_tickets)
            self._cash_line      += 1
            self._cash_line_wait += time_offset
        elif chosen_line == TicketBooth.EXPRESS:
            desired_tickets = 200 # Express line always gives 200 tickets
            time_offset     =  15 # Express line always takes 15 seconds
            self._express_line      += 1
            self._express_line_wait += time_offset
        else:
            raise RuntimeError("Unreachable code reached")

        assert desired_tickets > 0

        next_event = event.GetTickets(
            customer=customer,
            time=time + (
                self._cash_line_wait
                if chosen_line == TicketBooth.CASH
                else self._express_line_wait
            ),
            num_tickets=desired_tickets,
            line=chosen_line
        )

        # If the sale would occur after the ticket booth closes, push a
        # RemovedFromLine event instead of a GetTickets event.
        if next_event.get_time() >= TicketBooth.CLOSING_TIME:
            next_event = event.RemovedFromLine(
                time=TicketBooth.CLOSING_TIME,
                customer=customer,
                line=chosen_line
            )
            self.logger.debug('%s is too late to get tickets', customer)
        else:
            self.logger.debug(
                '%s entered line %s (%d) at %s to buy tickets at %s',
                customer,
                'cash' if chosen_line == TicketBooth.CASH else 'express',
                self._cash_line
                    if chosen_line == TicketBooth.CASH
                    else self._express_line,
                util.sec_to_tod(time),
                util.sec_to_tod(next_event.get_time())
            )

        return next_event
    # End of enter_line()

    def _choose_line(self, customer):
        # Early customers want express line 80% of the time; late customers 10%
        express_line_pref = 0.80 if customer.is_early() else 0.10
        chosen_line = TicketBooth.EXPRESS if rand() < express_line_pref \
            else TicketBooth.CASH
        # If the cash line is twice as long as the express line and the wait
        # time is more than 20 minutes, 20% will switch to express.
        if chosen_line == TicketBooth.CASH \
        and self.cash_line_wait_estimate() > 20 \
        and self.cash_line_length() > 2 * self.express_line_length() \
        and rand() < 0.20:
            chosen_line = TicketBooth.EXPRESS
        return chosen_line

    @staticmethod
    def _get_cash_desired_tickets():
        # Customers get X ~ N(100, 30) tickets
        desired_tickets = round(npr.normal(loc=100, scale=30))
        while desired_tickets <= 0:
            desired_tickets = round(npr.normal(loc=100, scale=30))
        assert desired_tickets > 0, 'Customer must get positive # tickets'
        return desired_tickets

    @staticmethod
    def _get_cash_time_offset(desired_tickets):
        # Takes 1 second to unspool 10 tickets
        unspooling_time = desired_tickets // 10
        # Average base time is 2 minutes, modeled by exponential
        base_time = npr.exponential(scale=120)
        return base_time + unspooling_time

    def sell_tickets(self, got_tickets):
        customer    = got_tickets.get_customer()
        line        = got_tickets.get_line()
        num_tickets = got_tickets.get_tickets()
        time        = got_tickets.get_time()

        # Give customer tickets
        customer.buy_tickets(num_tickets)
        self._tickets_sold += num_tickets
        revenue = 0.25 * num_tickets
        self._money_made += revenue

        # Remove customer from line
        if line == TicketBooth.CASH:
            assert self._cash_line > 0
            self._cash_line -= 1
        elif line == TicketBooth.EXPRESS:
            assert self._express_line > 0
            self._express_line -= 1
        else:
            raise RuntimeError('Got bad line: ' + str(line))

        self.logger.debug(
            '%s got %d tickets for $%.2f at %s',
            customer,
            num_tickets,
            revenue,
            util.sec_to_tod(time)
        )
    # End of give_tickets()

    def close(self):
        self.logger.info("Ticket booth closed")
        self._cash_line = 0
        self._express_line = 0
        self._is_open = False

    def is_open(self):
        return self._is_open

    def money_made(self):
        """Return how much money was made selling tickets."""
        return self._money_made

    def tickets_sold(self):
        return self._tickets_sold

    def people_in_line(self):
        return self._cash_line + self._express_line

    def cash_line_length(self):
        return self._cash_line

    def express_line_length(self):
        return self._express_line

    def cash_line_wait_estimate(self):
        """How long it takes to get tickets from the end of the cash line, in
        seconds, on average.
        """
        # On average, a customer takes 130 seconds to get his tickets.
        return 130 * self.cash_line_length()

    def cash_line_wait_actual(self):
        """How long it takes to get tickets from the end of the cash line, in
        seconds, in this particular cash line.
        """
        return self._cash_line_wait

    def express_line_wait(self):
        """How long it takes to get tickets from the end of the express line,
        in seconds.
        """
        return self.express_line_length() * 15

# End of Fair

### Miscellaneous

* [x] Fix the bug that triggers the assertion in `GetTickets.__init__()`

### Things to parameterize

- [x] Number of employees (excluding cashier)
- [x] Employee pay per day
- [x] S&G capacity
- [x] MGR capacity

### Stats to collect

- [x] **Money made** --- Calculate cost of employees per day
- [x] Express line lengths
- [x] Cash line lengths
- [x] Express line wait times
- [x] Cash line wait times
- [x] MGR runs
- [x] RC runs
- [x] Stalls & Games visitors
- [x] MGR visitors
- [x] RC visitors
- [x] Average line length for RC
- [x] Average line length for MGR
- [x] Number of customers at the fair at different times of the day
- [x] Total number of customers who visited the fair in a day
- [x] Average # of tickets wasted
- [ ] Average wait time for RC
- [ ] Average wait time for MGR
- [ ] How busy the employees are on average

### Experiments to conduct

- [x] Collect results on default settings
- [ ] Improve ticket selling by:
  - [ ] Adding more cash lines
  - [ ] Adding more express lines (\$1,000 to add each)
- [x] Tweak merry-go-round by:
  - [x] Renting a bigger merry-go-round (capacity 20 -> 30) for \$5,000
  - [x] Downgrading to a smaller merry-go-round (capacity 20 -> 15) to save
  \$2,000
- [x] Tweak stalls & games by:
  - [x] Adding more stalls --- \$500 per stall plus \$80 a day for each new
  stall operator, +5 capacity
  - [x] Removing stalls --- \$200 per stall sold, lay off one employee

### Report

- [x] Document experimental design
- [x] Analyze results for simulations with different parameters
  - [x] How does changing the merry-go-round's capacity affect profits?
  - [x] How does changing the number of stalls affect tickets wasted?
  - [x] How does changing the number of stalls affect profits?

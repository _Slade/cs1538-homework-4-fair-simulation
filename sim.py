"""Run the simulation.

Base unit of time: minutes.

"""

import argparse
import json
import logging
import numpy
import sys
import time

import attractions
import util

from attractions import RollerCoaster, StallsAndGames, MerryGoRound
from event import EventQueue, \
    Arrival,          \
    CustomerEvent,    \
    FairClose,        \
    GetTickets,       \
    RemovedFromLine,  \
    RideFinished,     \
    TicketBoothClose, \
    TimeEvent,        \
    VisitAttraction
from fair import Fair
from fair import TicketBooth
from util import tod_to_sec, sec_to_tod

LOGGER = None

def getopts():
    defaults = {
        'log_level'                 : 'WARNING',
        'days'                      : 100,
        'early_customer_num'        : 200,
        'early_customer_mean'       : 0,
        'early_customer_std_dev'    : 15 * 60,
        'late_customer_rate'        : 15 / (60*60), # 15 per hour
        'employee_num'              : 12, # No. of employees excluding cashier
        'employee_pay'              : 10, # Dollars/day
        'stalls-and-games-capacity' : StallsAndGames.DEFAULT_CAPACITY,
        'merry-go-round-capacity'   : MerryGoRound.DEFAULT_CAPACITY,
        'save-full-stats'           : False
    }

    parser = argparse.ArgumentParser(description='Simulate a fair')

    parser.set_defaults(**defaults)

    parser.add_argument(
        '--days',
        type=util.positive_int_arg,
        dest='days',
        help='Number of days to simulate. (default: 100)'
    )
    parser.add_argument(
        '--early-customer-num',
        type=util.positive_int_arg,
        dest='early_customer_num',
        help='Number of early customers who arrive. (default: 200)'
    )
    parser.add_argument(
        '--early-customer-mean',
        type=util.positive_arg,
        dest='early_customer_mean',
        help='The mean arrival time for an early customer in seconds.'
             ' (default: 0 seconds from opening of fair)'
    )
    parser.add_argument(
        '--early-customer-std-dev',
        type=util.positive_arg,
        dest='early_customer_std_dev',
        help='The standard deviation for early customer arrival time '
             ' in seconds. (default: 15 minutes = 900 seconds)'
    )
    parser.add_argument(
        '--late-customer-rate',
        type=util.positive_arg,
        dest='late_customer_rate',
        help='The average number of late customers who arrive per second.'
             ' (default: 15 per hour = 0.0041666... per second)'
    )
    parser.add_argument(
        '--employee-num',
        type=util.positive_int_arg,
        dest='employee_num',
        help='The number of employees who work at the fair each day, NOT'
             ' counting the cashier. (default: 12)'
    )
    parser.add_argument(
        '--employee-pay',
        type=util.positive_arg,
        dest='employee_pay',
        help='The hourly pay for each employee. (default: $10/h)'
    )
    parser.add_argument(
        '--stalls-and-games-capacity',
        type=util.positive_int_arg,
        dest='stalls_and_games_capacity',
        help='The capacity of stalls and games. (default: 50)'
    )
    parser.add_argument(
        '--merry-go-round-capacity',
        type=util.positive_int_arg,
        dest='merry_go_round_capacity',
        help='The capacity of the merry-go-round. (default: 20)'
    )
    parser.add_argument(
        '-l', '--log-level',
        choices='DEBUG INFO WARNING ERROR CRITICAL'.split(),
        dest='log_level',
        help='Set the global logging level. (default: WARNING)'
    )
    parser.add_argument(
        '--save-full-stats',
        type=bool,
        dest='save_full_stats',
        help='Save a file containing the full stats from the sim.'
             ' (default: False)'
    )
    return parser.parse_args()

def init(opts):
    global LOGGER
    level = util.get_log_level(opts.log_level)
    if level is None:
        level = logging.WARNING
    logging.basicConfig(
        level=level,
        format='\t|\t'.join([
            # '%(asctime)s',
            '%(levelname)s',
            '%(name)s:%(lineno)d',
            '%(message)s'
        ]),
        # datefmt='%Y-%m-%d %H:%M:%S'
    )
    LOGGER = logging.getLogger('sim')
    LOGGER.info('Initialized logger (level is %s)', opts.log_level)

# The simulation runs for 100 days. Each day begins at 10AM and runs until 6PM.
# A fair day is 480 minutes long. We'll simulate by minute.
def main():
    global LOGGER
    opts = getopts()
    init(opts)

    stats = []
    # Main event queue, holds events of different types.
    events = EventQueue()

    start_time = time.time()

    for day in range(0, opts.days):
        # Represents the fair
        fair = Fair(
            events=events,
            early_cust_num=opts.early_customer_num,
            early_cust_mean=opts.early_customer_mean,
            early_cust_std_dev=opts.early_customer_std_dev,
            late_cust_rate=opts.late_customer_rate
        )
        # Attractions
        rollercoaster    = RollerCoaster()
        stalls_and_games = StallsAndGames(capacity=opts.stalls_and_games_capacity)
        merry_go_round   = MerryGoRound(capacity=opts.merry_go_round_capacity)
        # Represents the ticket booth
        ticket_booth = TicketBooth()

        # For tracking how many customers are at the fair
        customers_present = 0

        # Initialize stats structure for this day
        stats.append({
            'cash_line_lengths'       : [],
            'express_line_lengths'    : [],
            'cash_line_wait_times'    : [],
            'express_line_wait_times' : [],
            'rc_line_lengths'         : [],
            'mgr_line_lengths'        : [],
            'customers_present'       : [],
            'num_customers'           : 0,
            'rc_visitors'             : 0,
            'mgr_visitors'            : 0,
            's&g_visitors'            : 0,
            'rc_runs'                 : 0,
            'mgr_runs'                : 0,
            'tickets_sold'            : 0,
            'revenue'                 : 0,
            'expense'                 : 0,
            'tickets_wasted'          : 0,
            'customers_too_late'      : 0,
        })

        # Start by populating the event queue with hour markers for stat
        # collection.
        for hour in [ 60 * 60 * t for t in range(0, 8) ]:
            events.push(TimeEvent(hour))

        events.push(FairClose(time=tod_to_sec('06:00:00 PM')))

        # Set up ticket booth closing event
        events.push(TicketBoothClose(time=TicketBooth.CLOSING_TIME))
        LOGGER.info(
            'Ticket booth closes at %s',
            util.sec_to_tod(TicketBooth.CLOSING_TIME)
        )

        while events:
            event = events.pop()

            if isinstance(event, TicketBoothClose):
                ticket_booth.close()

            elif isinstance(event, TimeEvent):
                # Collect hourly statistics
                LOGGER.info(
                    '-'*32 + ' %s ' + '-'*32,
                    sec_to_tod(event.get_time())
                )
                LOGGER.info(
                    '%d customer%s present',
                    customers_present,
                    's' if customers_present != 1 else ''
                )
                LOGGER.info(
                    '%d %s in line at ticket booth',
                    ticket_booth.people_in_line(),
                    'person' if ticket_booth.people_in_line() == 1
                        else 'people'
                )
                LOGGER.info(
                    '%s in line for rollercoaster',
                    rollercoaster.get_line_length()
                )
                LOGGER.info(
                    '%s in line for merry-go-round',
                    merry_go_round.get_line_length()
                )
                LOGGER.info('-'*72)
                stats[day]['cash_line_lengths'] \
                    .append(ticket_booth.cash_line_length())
                stats[day]['express_line_lengths'] \
                    .append(ticket_booth.express_line_length())
                stats[day]['cash_line_wait_times'] \
                    .append(ticket_booth.cash_line_wait_estimate())
                stats[day]['express_line_wait_times'] \
                    .append(ticket_booth.express_line_wait())
                stats[day]['rc_line_lengths'] \
                    .append(rollercoaster.get_line_length())
                stats[day]['mgr_line_lengths'] \
                    .append(merry_go_round.get_line_length())
                stats[day]['customers_present'] \
                    .append(customers_present)
                ... # TODO

            elif isinstance(event, Arrival):
                LOGGER.debug(
                    '%s arrived %s at %s',
                    event.get_customer(),
                    'early' if event.get_customer().is_early() else 'late',
                    sec_to_tod(event.get_time())
                )
                next_event = ticket_booth.enter_line(
                    event.get_customer(),
                    event.get_time()
                )
                customers_present += 1
                events.push(next_event)

            elif isinstance(event, GetTickets):
                ticket_booth.sell_tickets(event)
                events.push(
                    VisitAttraction(
                        customer=event.get_customer(),
                        time=event.get_time() + 1
                    )
                )

            elif isinstance(event, RemovedFromLine):
                stats[day]['customers_too_late'] += 1
                LOGGER.debug(
                    '%s was too late to buy tickets and left the fair',
                    event.get_customer()
                )
                customers_present -= 1
                assert customers_present >= 0

            elif isinstance(event, VisitAttraction):
                # Choose an attraction
                LOGGER.debug(
                    '%s is picking an attraction at %s (%d ticket%s remaining)',
                    event.get_customer(),
                    sec_to_tod(event.get_time()),
                    event.get_customer().tickets_held(),
                    's' if event.get_customer().tickets_held() != 1 else ''
                )
                attraction = attractions.random_attraction(
                    rollercoaster=rollercoaster,
                    stalls_and_games=stalls_and_games,
                    merry_go_round=merry_go_round,
                    tickets=event.get_customer().tickets_held()
                )
                # Customers leave if they can no longer afford any attractions,
                # or if they can only afford S&G and S&G is full.
                if attraction is None or (
                    attraction is stalls_and_games
                    and event.get_customer().tickets_held()
                        < merry_go_round.get_cost()
                    and stalls_and_games.get_occupants()
                        >= stalls_and_games.get_capacity()
                ):
                    LOGGER.debug(
                        "%s left the fair at %s with %d tickets remaining",
                        event.get_customer(),
                        sec_to_tod(event.get_time()),
                        event.get_customer().tickets_held()
                    )
                    customers_present -= 1
                    stats[day]['tickets_wasted'] \
                        += event.get_customer().tickets_held()
                else:
                    assert event.get_customer().tickets_held() \
                        >= attraction.get_cost()
                    next_event = attraction.get_on_ride(
                        event.get_customer(),
                        event.get_time()
                    )
                    if next_event is not None:
                        assert isinstance(
                            next_event,
                            (RideFinished, VisitAttraction)
                        )
                        events.push(next_event)

            elif isinstance(event, RideFinished):
                for next_event in event.get_attraction().finish_run(event):
                    events.push(next_event)

            elif isinstance(event, FairClose):
                LOGGER.info('Fair closed at %s', sec_to_tod(event.get_time()))

                # Get wasted tickets
                customers = set()
                while events:
                    e = events.pop()
                    if isinstance(e, CustomerEvent):
                        customer = e.get_customer()
                        if customer is not None:
                            customers.add(customer)
                events.clear()
                for customer in customers:
                    stats[day]['tickets_wasted'] += customer.tickets_held()
                    customers_present -= 1
                    assert customers_present >= 0
                stats[day]['rc_visitors'] = rollercoaster.get_visitors()
                stats[day]['mgr_visitors'] = merry_go_round.get_visitors()
                stats[day]['s&g_visitors'] = stalls_and_games.get_visitors()
                stats[day]['rc_runs'] = rollercoaster.get_times_run()
                stats[day]['mgr_runs'] = rollercoaster.get_times_run()
                stats[day]['tickets_sold'] += ticket_booth.tickets_sold()
                stats[day]['revenue'] += ticket_booth.money_made()
                stats[day]['expense'] += (
                    # Cost for employes who work from 10 to 6
                    opts.employee_num * opts.employee_pay * 8
                    # Cost for cashier, who works from 10 to 5
                    + opts.employee_pay * 7
                )
                stats[day]['profit'] = stats[day]['revenue'] \
                    - stats[day]['expense']
                stats[day]['num_customers'] = fair.get_customer_count()
                assert all([ n <= stats[day]['num_customers']
                    for n in stats[day]['customers_present']
                ])
            # This shouldn't be reached.
            else:
                raise RuntimeError('Unhandled event: ' + str(event))
        # End of event loop

    LOGGER.info('Simulation complete (%f seconds)', time.time() - start_time)

    aggregates = {}

    for stat in [
        'cash_line_lengths',
        'express_line_lengths',
        'cash_line_wait_times',
        'express_line_wait_times',
        'rc_line_lengths',
        'mgr_line_lengths',
        'customers_present'
    ]:
        aggregates['avg_' + stat] = hourly_averages(opts.days, stats, stat)

    for stat in [
        'customers_too_late',
        'mgr_runs',
        'mgr_visitors',
        'num_customers',
        'rc_runs',
        'rc_visitors',
        'tickets_sold',
        'revenue',
        'expense',
        'profit',
        's&g_visitors',
        'tickets_wasted'
    ]:
        aggregates['avg_' + stat] = daily_average(opts.days, stats, stat)
        aggregates['std_' + stat] = daily_stddev(opts.days, stats, stat)

    aggregates['total_profit'] = sum_stat(opts.days, stats, 'profit')

    if opts.save_full_stats:
        with open(str(time.time()) + '_results.json', 'w') as fp:
            json.dump(stats, fp)
    json.dump(aggregates, sys.stdout, indent=2)

# End of main()

def daily_average(days, stats, stat):
    return numpy.mean([stats[day][stat] for day in range(0, days)])

def daily_stddev(days, stats, stat):
    return numpy.std([stats[day][stat] for day in range(0, days)], ddof=1)

def hourly_averages(days, stats, stat):
    return list(
        numpy.mean(
            numpy.array([stats[day][stat] for day in range(0, days)]),
            axis=0
        )
    )

def sum_stat(days, stats, stat):
    return sum(stats[day][stat] for day in range(0, days))

if __name__ == '__main__':
    main()

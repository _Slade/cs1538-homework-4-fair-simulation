#!env sh

if [[ !( $(python -V) = 'Python 3'* ) ]]; then
    echo Please run this script with Python 3 in your path
    exit
fi

trap 'for job in $(jobs -p); do kill "$job"; done' INT TERM
let FAIL=0

echo Simulating default
python -OO sim.py > default.json &
echo Simulating reduced MGR size
python -OO sim.py --merry-go-round-capacity=15 > mgr_15.json &
echo Simulating increased MGR size
python -OO sim.py --merry-go-round-capacity=30 > mgr_30.json &
for employee in {-5..5}; do
    [[ $employee == 0 ]] && continue
    echo "Simulating $employee stalls and games employee(s)"
    python -OO sim.py \
        --employee-num=$(( 12 + $employee )) \
        --stalls-and-games-capacity=$(( 50 + $employee * 5)) \
        > sg_$employee.json &
done

for job in $(jobs -p); do
    echo $job
    wait $job || let "FAIL+=1"
done

echo $FAIL

if [[ "$FAIL" == "0" ]]; then
    echo "All simulations successful"
else
    echo "$FAIL simulation(s) failed"
fi
